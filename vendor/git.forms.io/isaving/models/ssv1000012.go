//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000012I struct {
	CustId string `valid:"Required" description:"Client number"` //客户号
}

type SSV1000012O struct {
	//CustomerName string //客户名称
	//LastName     string //姓氏
	//FirstName    string //名称
	//IdNation     string //发证国家
	//IdType       string //证件类型
	//IdNo         string //证件号码
	//Gender       string //性别
	//Birthdate    string //生日
	//Nation       string //民族
	//Marriage     string //婚姻情况
	CustStatus   string //客户状态   0-正常 1-死亡 2-破产 3-暂禁 4-待生效 9-注销
}

// @Desc Build request message
func (o *SSV1000012I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000012I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000012O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000012O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000012I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000012I) GetServiceKey() string {
	return "ssv1000012"
}
