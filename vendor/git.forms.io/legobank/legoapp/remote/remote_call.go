package remote

import (
	"git.forms.io/legobank/legoapp/config"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/comm-agent/client"
	common "git.forms.io/universe/comm-agent/common/protocol"
	"git.forms.io/universe/solapp-sdk/comm"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/log"
	"git.forms.io/universe/solapp-sdk/util"
	"github.com/astaxie/beego"
	"github.com/valyala/fasthttp"
	"strings"
	"time"
)

var remoteCall = comm.NewRemoteClientFactory().CreateClient()

type RemoteCallInterface interface {
	RequestSyncServiceWithDCN(dtsCtx *compensable.TxnCtx, srcAppProps map[string]string, dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error)
	RequestSyncServiceElementKey(dtsCtx *compensable.TxnCtx, srcAppProps map[string]string, elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error)
	RequestAsyncServiceWithDCN(srcAppProps map[string]string, dstDcn, serviceKey string, requestData []byte) (err error)
	RequestAsyncServiceElementKey(srcAppProps map[string]string, elementType, elementId, serviceKey string, requestData []byte) (err error)
}

type DefaultRemoteCallImpl struct{}

func requestAsyncService(dtsCtx *compensable.TxnCtx,
	dstDcn, serviceKey string,
	requestData []byte, srcAppProps map[string]string) error {

	if nil == srcAppProps {
		srcAppProps = make(map[string]string)
	}
	srcAppProps[constant.SRCBIZSEQNO] = util.GenerateSerialNo("1")

	request := &client.UserMessage{
		AppProps: srcAppProps,
		Body:     requestData,
	}

	topic := beego.AppConfig.String(serviceKey + "::mesh")
	if err := remoteCall.AsyncCall(dtsCtx, config.ServiceConf.Organization, dstDcn, serviceKey, request); err != nil {
		return errors.Errorf(constant.REMOTEFAILD, "Remote call %s failed, error: %v", topic, err)
	}

	log.Debugf("End requestAsyncService remoteCall topic: [%v]", topic)

	return nil
}

func requestSyncService(dtsCtx *compensable.TxnCtx,
	dstDcn, serviceKey string,
	requestData []byte, srcAppProps map[string]string) (responseData []byte, err error) {

	var dcn string
	if dstDcn == config.ServiceConf.CommonDcn {
		dcn = ""
	} else if "" != dstDcn {
		dcn = dstDcn
	}
	if nil == srcAppProps {
		srcAppProps = make(map[string]string)
	}
	srcAppProps[constant.SRCBIZSEQNO] = util.GenerateSerialNo("1")

	request := &client.UserMessage{
		AppProps: srcAppProps,
		Body:     requestData,
	}
	response := &client.UserMessage{}

	maxRetryTimes := beego.AppConfig.DefaultInt(serviceKey+"::maxRetryTimes", 0)
	maxAutoRetryTimes := beego.AppConfig.DefaultInt(serviceKey+"::maxAutoRetryTimes", 0)
	timeoutMilliseconds := beego.AppConfig.DefaultInt(serviceKey+"::timeoutMilliseconds", 30000)
	waitingMilliseconds := beego.AppConfig.DefaultInt(serviceKey+"::waitingMilliseconds", 5000)
	topic := beego.AppConfig.String(serviceKey + "::mesh")
	serviceKeySection, err := beego.AppConfig.GetSection(serviceKey)
	if nil == err {
		if _, ok := serviceKeySection["is_need_lookup"]; ok {
			request.AppProps[constant.IS_NEED_LOOKUP] = serviceKeySection["is_need_lookup"]
		}
	}

	tryCnt := 0
	for {
		tryCnt++
		err := remoteCall.SyncCallV2(dtsCtx, config.ServiceConf.Organization, dcn, serviceKey,
			request, response, timeoutMilliseconds, maxAutoRetryTimes)

		retMsgCode := response.AppProps[constant.RETMSGCODE]
		if err == nil && retMsgCode != constant.SYTIMEOUT && retMsgCode != constant.SYERRCONT {
			break
		}
		if err != nil && err != comm.ErrTimeout && strings.Contains(err.Error(), "redis: nil") &&
			err != fasthttp.ErrConnectionClosed && err != fasthttp.ErrTimeout && err != fasthttp.ErrNoFreeConns {
			log.Errorf("Request target service [%s] failed error [%v], GlobalBizSeqNo [%s], SrcBizSeqNo [%s]",
				serviceKey, err, request.AppProps[constant.GLOBALBIZSEQNO], request.AppProps[constant.SRCBIZSEQNO])
			return nil, errors.Errorf(constant.REMOTEFAILD, "Remote call %s failed, error: %v", topic, err)
		}
		if tryCnt > maxRetryTimes {
			log.Errorf("Request target service [%s] failed error [%v], GlobalBizSeqNo [%s], SrcBizSeqNo [%s]",
				serviceKey, err, request.AppProps[constant.GLOBALBIZSEQNO], request.AppProps[constant.SRCBIZSEQNO])
			return nil, errors.Errorf(constant.REMOTEFAILD, "Remote call %s failed, error: %v", topic, err)
		}
		log.Errorf("Request target service [%s] timeout, try counts [%d], try again, GlobalBizSeqNo [%s], SrcBizSeqNo [%s]...",
			serviceKey, tryCnt, request.AppProps[constant.GLOBALBIZSEQNO], request.AppProps[constant.SRCBIZSEQNO])
		time.Sleep(time.Duration(waitingMilliseconds) * time.Millisecond)
	}
	log.Debugf("End remoteCall topic: [%v], responseAppProps: [%++v], Body: [%v]", topic, response.AppProps, string(response.Body))
	updateAppProps(request.AppProps, response.AppProps)
	if response.AppProps[constant.RETSTATUS] == "F" {
		return response.Body, errors.New(response.AppProps[constant.RETMESSAGE], response.AppProps[constant.RETMSGCODE])
	}

	return response.Body, nil
}

func updateAppProps(appProps, rspAppProps map[string]string) {
	appProps[constant.TRGBIZSEQNO] = rspAppProps[constant.SRCBIZSEQNO]
	appProps[constant.LASTACTENTRYNO] = rspAppProps[constant.LASTACTENTRYNO]
	appProps[common.DLS_ELEMENT_TYPE] = ""
	appProps[common.DLS_ELEMENT_ID] = ""
}

func checkServiceKey(serviceKey string) (topic string, err error) {

	topic = beego.AppConfig.String(serviceKey + "::mesh")
	if topic == "" {
		return "", errors.Errorf(constant.NOTFOUNDTOPIC, "Can't found destination topic. serviceKey: [%v]", serviceKey)
	}
	return topic, nil
}

func (d *DefaultRemoteCallImpl) RequestAsyncServiceWithDCN(srcAppProps map[string]string,
	dstDcn, serviceKey string, requestData []byte) (err error) {
	if topic, err := checkServiceKey(serviceKey); err != nil {
		return err
	} else {
		log.Debugf("Start RequestAsyncServiceWithDCN topic [%v], destDcn [%v], srcAppProps [%++v], requestData [%v]",
			topic, dstDcn, srcAppProps, string(requestData))
	}
	return requestAsyncService(nil, dstDcn, serviceKey, requestData, srcAppProps)
}

func (d *DefaultRemoteCallImpl) RequestAsyncServiceElementKey(srcAppProps map[string]string,
	elementType, elementId,
	serviceKey string,
	requestData []byte) (err error) {
	if topic, err := checkServiceKey(serviceKey); err != nil {
		return err
	} else {
		log.Debugf("Start RequestAsyncServiceElementKey topic [%v], elementType [%v], elementId [%v], srcAppProps [%++v], requestData [%v]",
			topic, elementType, elementId, srcAppProps, string(requestData))
	}

	if srcAppProps == nil {
		srcAppProps = make(map[string]string)
	}
	srcAppProps[common.DLS_ELEMENT_TYPE] = elementType
	srcAppProps[common.DLS_ELEMENT_ID] = elementId
	srcAppProps[common.TARGET_DCN] = ""

	return requestAsyncService(nil, "", serviceKey, requestData, srcAppProps)
}

func (d *DefaultRemoteCallImpl) RequestSyncServiceWithDCN(dtsCtx *compensable.TxnCtx,
	srcAppProps map[string]string, dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {
	if topic, err := checkServiceKey(serviceKey); err != nil {
		return nil, err
	} else {
		log.Debugf("Start RequestSyncServiceWithDCN topic [%v], destDcn [%v], srcAppProps [%++v], requestData [%v]",
			topic, dstDcn, srcAppProps, string(requestData))
	}
	return requestSyncService(dtsCtx, dstDcn, serviceKey, requestData, srcAppProps)
}

func (d *DefaultRemoteCallImpl) RequestSyncServiceElementKey(
	dtsCtx *compensable.TxnCtx,
	srcAppProps map[string]string,
	elementType, elementId,
	serviceKey string,
	requestData []byte) (responseData []byte, err error) {

	if topic, err := checkServiceKey(serviceKey); err != nil {
		return nil, err
	} else {
		log.Debugf("Start RequestSyncServiceElementKey topic [%v], elementType [%v], elementId [%v], srcAppProps [%++v], requestData [%v]",
			topic, elementType, elementId, srcAppProps, string(requestData))
	}

	if srcAppProps == nil {
		srcAppProps = make(map[string]string)
	}
	srcAppProps[common.DLS_ELEMENT_TYPE] = elementType
	srcAppProps[common.DLS_ELEMENT_ID] = elementId
	srcAppProps[common.TARGET_DCN] = ""
	srcAppProps[constant.IS_NEED_LOOKUP] = "1"


	return requestSyncService(dtsCtx, "", serviceKey, requestData, srcAppProps)
}
