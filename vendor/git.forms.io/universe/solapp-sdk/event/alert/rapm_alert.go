//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package alert

type RapmAlertEvent struct {
	Level     string `json:"level"`
	Content   string `json:"content"`
	AlertTime string `json:"alertTime"`
}
