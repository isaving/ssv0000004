//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package imports

import (
	"git.forms.io/universe/dts/client/event"
	"git.forms.io/universe/dts/client/routers"
	constant "git.forms.io/universe/dts/common/const"
	"git.forms.io/universe/dts/common/log"
)

// @Desc initialize routers for DTS
func EnableDTSSupports() (err error) {
	log.Infof("IsEnable DTS supports, version:%s", constant.DTS_SDK_VERSION)
	//if err = config.InitCmpServiceConfig(); err != nil {
	//	log.Errorf("Init compensable service config failed err=%v", err)
	//	return
	//}

	// initialize DTS router
	if err = routers.InitDTSRouters(); err != nil {
		log.Errorf("Init DTS routers failed err=%v", err)
		return
	}

	// initialize call-back router
	if err := event.InitEventRouters(); nil != err {
		log.Errorf("InitEventRouters failed err=%v", err)
		return err
	}

	return
}
