//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package constant

const (
	DTS_SDK_VERSION = "v0.2.1-beta.3"
)

const (
	COMM_DIRECT                    = "direct"
	COMM_MESH                      = "mesh"
	PARTICIPANT_ADDRESS_SPLIT_CHAR = "|"
	TOPIC_ID_SPLIT_CHAT            = "/"
)

type TxnType int

const (
	TXN_ROOT TxnType = iota
	TXN_BRANCH
)

const (
	RECORD_STATUS_CREATED = "created"
	RECORD_STATUS_REMOVED = "removed"
)

/* transaction mode  start */
type TxnMode int

const (
	_ TxnMode = iota
	SOA
	EDA
)

/* dts server */
const (
	TXN_REGISTER_TOPIC_ID           = "DTS_AGENT_REGISTER"
	TXN_ENLIST_TOPIC_ID             = "DTS_AGENT_ENLIST"
	TXN_TRY_RESULT_REPORTY_TOPIC_ID = "DTS_AGENT_TRY_RESULT_REPORT"
	GLOBAL_TXN_RESULT               = "DTS_AGENT_GLOBAL_TXN_RESULT"
	TXN_CALLBACK_NODE_ID            = "NODE0001"
)

/* transaction mode end */

const (
	TXN_PROPAGATE_ROOT_XID_KEY      = "ROOT_XID"
	TXN_PROPAGATE_PARENT_XID_KEY    = "PARENT_XID"
	TXN_PROPAGATE_DTS_AGENT_ADDRESS = "DTS_AGENT_ADDRESS"
)

/*topic*/
const (
	DTS_TOPIC_TYPE   = "DTS"
	TRN_TOPIC_TYPE   = "TRN"
	TOPIC_SPLIT_CHAR = "/"
)

type TxnStat int

const (
	TRYING TxnStat = iota
	TRIED_OK
	TRIED_FAILED
	CONFIRMING
	CONFIRMED_OK
	CONFIRMED_FAILED
	CANCELLING
	CANCELLED_OK
	CANCELLED_FAILED
)

const (
	MESSAGE_TYPE_JSON = "json"
	MESSAGE_OK        = "ok"
)

const (
	TOPIC_KEY = "TOPIC"
)

const (
	BASE_CONTROLLER_FIELD_NAME = "DTSServerHandler"
	MESH_REQ_FIELD_NAME        = "Req"
	MESH_RSP_FIELD_NAME        = "Rsp"
	DTS_CTX_FIELD_NAME         = "DTSCtx"
)

const (
	ENV_NODE_ID_KEY     = "NODE_ID"
	ENV_INSTANCE_ID_KEY = "INSTANCE_ID"
)

const (
	CANNOT_FOUND_ROOT_XID_ERROR_CODE = -99
	DEFAULT_MAX_CALLBACK_CHANNEL     = 1024
)

const (
	TRACE_ID_KEY       = "GlobalBizSeqNo"
	SPAN_ID_KEY        = "SrcBizSeqNo"
	PARENT_SPAN_ID_KEY = "ParentBizSeqNo"
)

const (
	Success         = 0
	ErrorCodeOffset = 230000
	DTSErrorCode    = ErrorCodeOffset

	// 230001:uncaught exception error
	UnCaughtExceptionError = ErrorCodeOffset + 1

	// 230002:internal error
	InternalError = ErrorCodeOffset + 2

	// 230100:common error
	CommonError = ErrorCodeOffset + 100 //

	// 230101:invalid parameter error
	InvalidParameter = CommonError + 1

	// 230102:Transaction registration failed, root transaction already exists
	RegisterFailedRootXidAlreadyExists = CommonError + 2

	// 230103:Transaction registration failed, root transaction does not exist
	EnlistFailedCannotFindRootXid = CommonError + 3

	// 230104:Transaction registration failed, branch transaction already exists
	EnlistFailedBranchXidAlreadyExists = CommonError + 4

	// 230105:Transaction registration failed, transaction status error
	EnlistFailedGlobalTxnStateError = CommonError + 5

	// 230106:Global transaction result report failed, root transaction does not exist
	TryResultReportFailedCannotFindRootXid = CommonError + 6

	// 230107:Failed to report global transaction results, the transaction status is wrong
	TryResultReportFailedGlobalTxnStateError = CommonError + 7

	// 230108:Global transaction result report failed, branch transaction confirm failed
	TryResultReportFailedBranchConfirmFailed = CommonError + 8

	// 230109:Global transaction result report failed, branch transaction cancel failed
	TryResultReportFailedBranchCancelFailed = CommonError + 9 // :

	// 230110:Branch transaction confirm failed, branch transaction does not exist
	BranchTxnConfirmFailedCannotFindBranchXid = CommonError + 10

	// 230111:Branch transaction confirm failed, status of branch transaction is invalid
	BranchTxnConfirmFailedBranchTxnStateError = CommonError + 11

	// 230112:Branch transaction cancel failed, branch transaction does not exist
	BranchTxnCancelFailedCannotFindBranchXid = CommonError + 12

	// 230113:Branch transaction cancel failed, status of branch transaction if invalid
	BranchTxnCancelFailedBranchTxnStateError = CommonError + 13
)
