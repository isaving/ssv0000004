//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package client

import (
	"fmt"
	"git.forms.io/universe/common/json"
	"github.com/go-errors/errors"
	"strings"

	log "git.forms.io/universe/comm-agent/common/log"
	"git.forms.io/universe/common/try"
)

// define Dls Url Path
const (
	//url path
	DLS_URL_LOOK_LIST_PATH   = "/v1/dls_query/looklist"
	DLS_URL_LOOKDCNTYPE_PATH = "/v1/dls_query/lookdcntype"
	DLS_URL_LOOKUP_PATH      = "/v1/dls_query/lookup"
	DLS_URL_LOOKUPS_PATH     = "/v1/dls_query/lookups"
)

// define Dls Operate Interface
type DlsInterface interface {
	DlsLooklist(element *DlsElement) ([]DlsPrimaryDcn, uint, error)
	DlsLookDcnType(topicType, topicId string) (string, uint, error)
	DlsLookup(dim DlsDimension, element *DlsElement) (*DlsPrimaryDcn, uint, error)
	DlsLookups(dim DlsDimension, elements []DlsElement) (*DlsLookupsStruct, uint, error)
}

// define Dls Return Code
var (
	DLS_SUCCESSFUL uint = 0
	DLS_PARAM_NULL uint = 661400
	DLS_NOT_FOUND  uint = 661403
	DLS_SYS_ERR    uint = 669999
)

// @Desc define Dls Lookup Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsLookupStruct struct {
	Elements []DlsElement
}

// @Desc define check Dls Exist Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsExistStruct struct {
	Dimesion DlsDimension
	Element  DlsElement
}

// @Desc define Dls Element Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsElement struct {
	ElementType  string
	ElementClass string
	ElementId    string
}

// @Desc define Dls Topic Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsTopic struct {
	TopicType string
	TopicId   string
}

// @Desc define Dls Dimension Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsDimension struct {
	Topic   DlsTopic
	DcnType string
}

// @Desc define Dls Lookup Args Struct
// @Author zhuopu
// @Date 2020-05-12
type DlsDcnListArgs struct {
	DcnTypes []DlsDimension
}

// @Desc define Dls Lookup Args Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsLookupArgs struct {
	Dimesion *DlsDimension
	DlsElement
}

// @Desc define Dls Lookups Args Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsLookupsArgs struct {
	Dimesion *DlsDimension
	Elements []DlsElement
}

// @Desc define Dls Lookups Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsLookupsStruct struct {
	Total   uint
	Succ    uint
	PrmDcns []DlsPrimaryDcn
}

// @Desc define Dls PrimaryDcn Result Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsPrimaryDcnResult struct {
	DlsCode uint
	*DlsPrimaryDcn
}

// @Desc define Dls PrimaryDcn Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsPrimaryDcn struct {
	DcnType string
	DcnId   string
}

// @Desc define Dls Process Operate Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsProcessOperate struct {
	Type int
	Cmd  string
	Args []interface{}
}

// @Desc define Dls Update Element Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsUpdateElement struct {
	DlsElement
	SourceClass string
	SourceId    string
	State       int
}

// @Desc define Dls Process Primary Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsProcessPrimaryStruct struct {
	oper     *DlsProcessOperate
	elements []DlsElement
}

// @Desc define Dls Create Primary Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsCreatePrimaryStruct struct {
	Option   int
	Dimesion *DlsDimension
	Elements []DlsElement
}

// @Desc define Dls Update Primary Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsUpdatePrimaryStruct struct {
	Primary  *DlsElement
	Dimesion *DlsDimension
	Elements []DlsUpdateElement
}

// Reply Struct
// @Desc define Dls Reply Lookup Primary List Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsReplyLookupPrimaryList struct {
	Code    uint
	Message string
	Data    []DlsPrimaryDcn
}

// Reply Struct
// @Desc define Dlsreply Look Dcntype Primary Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsreplyLookDcntypePrimary struct {
	Code    uint
	Message string
	Data    string
}

// @Desc define Dls Reply Lookup Primary Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsReplyLookupPrimary struct {
	Code    uint
	Message string
	Data    *DlsPrimaryDcnResult
}

// @Desc define Dls Reply Lookups Primary Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsReplyLookupsPrimary struct {
	Code    uint
	Message string
	Data    *DlsLookupsStruct
}

// @Desc define Dls Reply Bool Result Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsReplyBoolResult struct {
	Code    uint
	Message string
	Data    *bool
}

// @Desc define Dls Reply Create Primary Struct
// @Author zhuopu
// @Date 2019-10-16
type DlsReplyCreatePrimary struct {
	Code    uint
	Message string
	Data    *DlsPrimaryDcn
}

type DlsOperate struct{}

// @Desc Search Dcn List by DlsElement
// @Author zhuopu
// @Date 2019-10-16
//
// @Param element DlsElement
// @Return []DlsPrimaryDcn, uint, error
func (op *DlsOperate) DlsLooklist(element *DlsElement) ([]DlsPrimaryDcn, uint, error) {
	// 校验参数
	s, _ := json.Marshal(element)
	log.Debugf("DlsLooklist elements: %v", s)
	if len(strings.Trim(element.ElementId, " ")) == 0 || len(strings.Trim(element.ElementType, " ")) == 0 {
		return nil, DLS_PARAM_NULL, fmt.Errorf("DlsLooklist,Dls Lookup Elements Paramer Error !")
	}

	rly := DlsReplyLookupPrimaryList{}
	if err := postDlsUserRequest(DLS_URL_LOOK_LIST_PATH, element, &rly); err != nil {
		log.Debugf("DlsLooklist Faild: err=%v", err)
		return nil, DLS_SYS_ERR, err
	}
	return rly.Data, rly.Code, nil
}

// @Desc Search DcnType by Topic Info
// @Author zhuopu
// @Date 2019-10-16
//
// @Param opicType, topicId string
// @Return string, uint, error
func (op *DlsOperate) DlsLookDcnType(topicType, topicId string) (string, uint, error) {
	// 校验参数
	dim := DlsDimension{
		Topic: DlsTopic{
			TopicType: topicType,
			TopicId:   topicId,
		},
		DcnType: "",
	}
	s, err := json.Marshal(dim)
	if err != nil {
		log.Debugf("DlsLookDcnType Marshal[%v] Faild: err=%v", dim, err)
		fmt.Println(err)
		return "", DLS_SYS_ERR, err
	}
	log.Debugf("DlsLookDcnType: %v", s)
	//if 0 == len(strings.Trim(dim.Topic.TopicType, " ")) && 0 == len(strings.Trim(dim.Topic.TopicType, " ")) {
	if 0 == len(strings.Trim(dim.Topic.TopicType, " ")) && 0 == len(strings.Trim(dim.Topic.TopicId, " ")) {
		return "", DLS_PARAM_NULL, err
	}

	rly := DlsreplyLookDcntypePrimary{}
	if err := postDlsUserRequest(DLS_URL_LOOKDCNTYPE_PATH, dim, &rly); err != nil {
		log.Debugf("DlsLookDcnType Faild: err=%v", err)
		return "", DLS_SYS_ERR, err
	}
	return rly.Data, rly.Code, nil
}

func postDlsUserRequest(path string, req interface{}, resp interface{}) error {
	err := postUserRequest(path, req, resp)
	switch err.(type) {
	case *errors.Error:
		{
			if err.(*errors.Error) != nil {
				err = err.(*errors.Error).Err
			} else {
				err = nil
			}
		}
	default:
		err = nil
	}
	return err
}

// @Desc Search Dcn by Dls Element and Topic Info
// @Author zhuopu
// @Date 2019-10-16
//
// @Param dim DlsDimension, element *DlsElement
// @Return *DlsPrimaryDcn, uint, error
func (op *DlsOperate) DlsLookup(dim DlsDimension, element *DlsElement) (*DlsPrimaryDcn, uint, error) {
	// check Parameters
	s, _ := json.Marshal(element)
	log.Debugf("DlsLookup elements: %v", s)
	if len(strings.Trim(element.ElementId, " ")) == 0 || len(strings.Trim(element.ElementType, " ")) == 0 {
		return nil, DLS_PARAM_NULL, fmt.Errorf("Dls Lookup Elements Paramer Error !")
	}

	rly := DlsReplyLookupPrimary{}
	if err := postDlsUserRequest(DLS_URL_LOOKUP_PATH, DlsLookupArgs{
		Dimesion:   &dim,
		DlsElement: *element,
	}, &rly); err != nil {
		log.Debugf("DlsLookup Faild: err=%v", err)
		return nil, DLS_SYS_ERR, err
	}
	if rly.Code == 0 {
		return rly.Data.DlsPrimaryDcn, rly.Code, nil
	} else {
		return nil, rly.Code, fmt.Errorf(rly.Message)
	}
}

// @Desc Search Dcn List by Dls Element List and Topic Info
// @Author zhuopu
// @Date 2019-10-16
//
// @Param dim DlsDimension, elements []DlsElement
// @Return *DlsLookupsStruct, uint, error
func (op *DlsOperate) DlsLookups(dim DlsDimension, elements []DlsElement) (*DlsLookupsStruct, uint, error) {
	// check Parameters
	s, _ := json.Marshal(elements)
	log.Debugf("DlsLookups elements: %v", s)
	for _, element := range elements {
		if len(strings.Trim(element.ElementId, " ")) == 0 || len(strings.Trim(element.ElementType, " ")) == 0 {
			return nil, DLS_PARAM_NULL, fmt.Errorf("DlsLookups,Dls Lookup Elements Paramer Error !")
		}
	}

	rly := DlsReplyLookupsPrimary{}
	if err := postDlsUserRequest(DLS_URL_LOOKUPS_PATH, DlsLookupsArgs{
		Dimesion: &dim,
		Elements: elements,
	}, &rly); err != nil {
		log.Debugf("DlsLookups Faild: err=%v", err)
		return nil, DLS_SYS_ERR, err
	}
	if rly.Code == try.UNIVERSE_SUCC_CODE {
		return rly.Data, rly.Code, nil
	} else {
		return rly.Data, rly.Code, fmt.Errorf(rly.Message)
	}
}

func DlsLooklist(element *DlsElement) ([]DlsPrimaryDcn, uint, error) {
	dlsOperate := &DlsOperate{}
	return dlsOperate.DlsLooklist(element)
}
func DlsLookDcnType(topicType, topicId string) (string, uint, error) {
	dlsOperate := &DlsOperate{}
	return dlsOperate.DlsLookDcnType(topicType, topicId)
}
func DlsLookup(dim DlsDimension, element *DlsElement) (*DlsPrimaryDcn, uint, error) {
	dlsOperate := &DlsOperate{}
	return dlsOperate.DlsLookup(dim, element)
}
func DlsLookups(dim DlsDimension, elements []DlsElement) (*DlsLookupsStruct, uint, error) {
	dlsOperate := &DlsOperate{}
	return dlsOperate.DlsLookups(dim, elements)
}
