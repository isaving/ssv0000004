//Version: v0.0.1
package constant

//define error code
const ()

const (
	CONSTANTNUMBER0 = "0"
	CONSTANTNUMBER1 = "1"
	CONSTANTNUMBER2 = "2"
	CONSTANTNUMBER3 = "3"
)
const (
	DlsuDcnLists   = "DlsuDcnLists"   // dcnList
	DLSUPCREATEDTS = "DLSUPCREATEDTS" // dts
	DLSUPUPDATEDTS = "DLSUPUPDATEDTS" // dts
	DTS_TOPIC_TYPE = "DTS"            // dts
	TRN_TOPIC_TYPE = "TRN"            // trn
	DLS_ID_COMMON  = "common"         // 乐高公共ID
	DLS_LNM_COMMON = "ilcomm"         // 贷款公共ID
	DLS_TYPE_CUS   = "CUS"            // 客户切片类型  //存款客户号
	DLS_TYPE_PHN   = "PHN"            // 手机切片类型
	DLS_TYPE_CMM   = "CMM"            // 乐高公共切片类型
	DLS_TYPE_CNT   = "CNT"            // 合同号切片类型
	DLS_TYPE_DBT   = "DBT"            // 借据号切片类型
	DLS_TYPE_LNM   = "LNM"            // 贷款切片类型
	DLS_TYPE_IDN   = "IDN"            // 证件信息切片类型

	DLS_TYPE_AGM = "AGM" // 存款账户切片类型
	DLS_TYPE_SCM = "SCM" // 存款公共切片类型
)
const (
	SV100013 = "sv100013"
	SV100012 = "sv100012"
	SV900004 = "sv900004"
	SV900005 = "sv900005"
)

const (
	ERRCODE1="SV99000001"
	ERRCODE2="SV99000002"
	ERRCODE3="SV99000003"
	ERRCODE4="SV99000004"
	ERRCODE5="SV99000005"
	ERRCODE6="SV99000006"
	ERRCODE7="SV99000007"
	ERRCODE8="SV99000008"
	ERRCODE9="SV99000009"
	ERRCODE10="SV99000010"
	ERRCODE11="SV99000011"
	

	ERRCODE_IL9  = "SVCM000001"
	ERRCODE_IL12 = "SVCM000002"
)
