//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000013I struct {
	CustId      string `validate:"required" description:"Client number"` //客户号 Y
	Account     string `validate:"required" description:"Contract ID"`   //合约号 Y
	//AListContact []AListContactArr ` description:"Contact information array"`     //联系信息数组
	//AListAddr    []AListAddr       ` description:"Contact information array"`     //地址信息数组
	AccuntNme     string               `validate:"max=120" description:"Account name"`       //账户名称
	CstmrCntctPh  string               `validate:"max=20" description:"Mobile phone number"` //手机号码
	CstmrCntctAdd string               `validate:"max=200" description:"Contact address"`    //联系地址
	CstmrCntctEm  string               `validate:"max=200" description:"Mailbox"`            //邮箱
	//ObjPublic     SSV1000013IObjPublic ` description:"Transaction public information"`       //交易公共信息
}
type SSV1000013IObjPublic struct {
	BranchNo string
	TellerId string
}

type SSV1000013O struct {
	Status string
}

// @Desc Build request message
func (o *SSV1000013I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000013I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000013O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000013O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000013I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000013I) GetServiceKey() string {
	return "ssv1000013"
}
