//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000004OResponseBody struct {
	returnCode string
	returnMsg  string
	Data       SSV9000004O
}

type SSV9000004I struct {
	QryTpy string `validate:"required,max=5"` //查询类型
	//MediaType     string `valid:"MaxSize(4)"`  //介质类型
	//MediaNm       string `valid:"MaxSize(40)"` //介质号码
	AgreementID   string `validate:"required,max=30" description:"Contract ID"`  //合约号
	AgreementType string `validate:"required,max=5" description:"Contract type"` //合约类型	 10001-个人活期账户;10002-个人定期账户;10003-个人活期保证金账户;10004-个人定期保证金账户;10005-对公活期账户;10006-对公定期账户;10007-对公活期保证金账户;10008-对公定期保证金账户;10009-内部账户;20001-个人贷款账户;20002-对公贷款账户;30001-协议存款;30002-保证金;30003-同业存款;30004-智能通知存款;30005-法人账户透支;30006-集团账户;30007-协定存款;30008-收息账号;30009-活期存款转定期约定;30010-网银开户签约;30011-贷款;30012-贷款收费;30013-普通定期存款;30014-活期保证金存款;30015-定期保证金存款;30016-普通活期存款;40001-客户对账单;-
}

type SSV9000004O struct {
	RecordTotNum int
	Records      []SSV9000004ORecord
}

type SSV9000004ORecord struct {
	AgreementID     string //合约号
	AgreementType   string //合约类型
	Currency        string //币种
	CashtranFlag    string //钞汇标志
	AgreementStatus string //账户状态//合约状态
	AccOpnDt        string //开户日期
	CstmrCntctPh    string //手机号
	CstmrCntctAdd   string //联系地址
	CstmrCntctEm    string //邮箱
	AccAcount       string //核算账号

	CstmrId         string //客户编号
	FreezeType      string //冻结状态
	TmpryPrhibition string //暂禁状态
}

// @Desc Build request message
func (o *SSV9000004I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000004I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000004O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000004O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000004I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000004I) GetServiceKey() string {
	return "ssv9000004"
}
