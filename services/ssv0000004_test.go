package services

import (
	"git.forms.io/isaving/sv/ssv0000004/models"
	"github.com/go-errors/errors"
	"testing"
)

var sv900004Response = `{"errorCode":"0","errorCode":"success","response":{"RecordTotNum":1,"Records":[{"AgreementID":"123","AgreementType":"11111","Currency":"1","CashtranFlag":"1","AgreementStatus":"0","AccOpnDt":"0000-00-00","CstmrCntctPh":"3","CstmrCntctAdd":"3","CstmrCntctEm":"3","AccAcount":"","CstmrId":"","FreezeType":"0","TmpryPrhibition":"0","AccuntNme":"3"}]}}`
var sv100012Response = `{"errorCode":"0","errorCode":"success","response":{"CustStatus":"ok111"}}`
var sv900005Response = `{"errorCode":"0","errorCode":"success","response":{}}`
var sv100013Response = `{"errorCode":"0","errorCode":"success","response":{"Status":"ok1"}}`
var i = 0

func (impl *Ssv0000004Impl) RequestSyncServiceElementKey(
	elementType, elementId, serviceKey string,
	requestData []byte) (responseData []byte, err error) {
	if i == 0 {
		switch serviceKey {
		case "sv900004":
			responseData = []byte(sv900004Response)
		case "sv100012":
			responseData = []byte(sv100012Response)
		case "sv900005":
			responseData = []byte(sv900005Response)
		case "sv100013":
			responseData = []byte(sv100013Response)
		}
	} else if i == 1 {
		return nil, errors.Errorf("%v", "你好")
	} else {
		return nil, nil
	}
	return responseData, nil
}

func TestTryCommService(t *testing.T) {
	impl := &Ssv0000004Impl{
		Sv000004I: &models.SSV0000004I{

			AgreementID:   "123",
			AgreementType: "11111",
			AccuntNme:     "3",
			CstmrCntctPh:  "3",
			CstmrCntctAdd: "3",
			CstmrCntctEm:  "3",
		},
	}

	sv000004O, err := impl.TrySsv0000004(impl.Sv000004I)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(sv000004O)
	}

	for j := 1; j < 3; j++ {
		i = j
		sv000004O, err := impl.querySSV9000004()
		if err != nil {
			t.Log(err)
		} else {
			t.Log(sv000004O)
		}
		sv1000012O, err := impl.querySSV1000012("1203")
		if err != nil {
			t.Log(err)
		} else {
			t.Log(sv1000012O)
		}
		err = impl.updateSSV1000013("1203")
		if err != nil {
			t.Log(err)
		} else {
			t.Log("success")
		}

		err = impl.updateSSV9000005()
		if err != nil {
			t.Log(err)
		} else {
			t.Log("success")
		}
	}
}

func TestConfirmSsv0000004(t *testing.T) {
	impl := &Ssv0000004Impl{
		Sv000004I: &models.SSV0000004I{
			AgreementID:   "123",
			AgreementType: "11111",
			AccuntNme:     "3",
			CstmrCntctPh:  "3",
			CstmrCntctAdd: "3",
			CstmrCntctEm:  "3",
		},
	}

	sv000004O, err := impl.ConfirmSsv0000004(impl.Sv000004I)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(sv000004O)
	}
}

func TestCancelSsv0000004(t *testing.T) {
	impl := &Ssv0000004Impl{
		Sv000004I: &models.SSV0000004I{
			AgreementID:   "123",
			AgreementType: "11111",
			AccuntNme:     "3",
			CstmrCntctPh:  "3",
			CstmrCntctAdd: "3",
			CstmrCntctEm:  "3",
		},
	}

	sv000004O, err := impl.CancelSsv0000004(impl.Sv000004I)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(sv000004O)
	}
}
