//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/sv/ssv0000004/constant"
	"git.forms.io/isaving/sv/ssv0000004/models"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/comm-agent/client"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)

var Ssv0000004Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv0000004",
	ConfirmMethod: "ConfirmSsv0000004",
	CancelMethod:  "CancelSsv0000004",
}

type Ssv0000004 interface {
	TrySsv0000004(*models.SSV0000004I) (*models.SSV0000004O, error)
	ConfirmSsv0000004(*models.SSV0000004I) (*models.SSV0000004O, error)
	CancelSsv0000004(*models.SSV0000004I) (*models.SSV0000004O, error)
}

type Ssv0000004Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Sv900005O    *models.SSV9000005O //SV900005O
	Sv900005I    *models.SSV9000005I //SV900005I
	Sv900004O    *models.SSV9000004O //SV900004O
	Sv900004I    *models.SSV9000004I //SV900004I
	Sv100012O    *models.SSV1000012O //SV100012O
	Sv100012I    *models.SSV1000012I //SV100012I
	Sv000004O    *models.SSV0000004O //SV000004O
	Sv000004I    *models.SSV0000004I //SV000004I
	DlsInterface *client.DlsOperate
}

// @Desc Ssv0000004 process
// @Author
// @Date 2020-12-04
func (impl *Ssv0000004Impl) TrySsv0000004(ssv0000004I *models.SSV0000004I) (ssv0000004O *models.SSV0000004O, err error) {
	log.Debug("Start try ssv0000004")
	//合约信息查询
	querySSV9000004O, err := impl.querySSV9000004()
	if err != nil {
		return nil, err
	}

	if querySSV9000004O.Records == nil {
		return nil, errors.Errorf(constant.ERRCODE1, "Account does not exist! : %v", err)
	}

	//账户状态/合约状态   0-正常;1-销户计息;2-销户;3-不动户-   2-结清;3-解约
	if querySSV9000004O.Records[0].AgreementStatus == constant.CONSTANTNUMBER1 {
		return nil, errors.Errorf(constant.ERRCODE2, "Interest accrued on closed accounts : %v", err)
	} else if querySSV9000004O.Records[0].AgreementStatus == constant.CONSTANTNUMBER2 {
		return nil, errors.Errorf(constant.ERRCODE3, "Account has been cancelled : %v", err)
	} else if querySSV9000004O.Records[0].AgreementStatus == constant.CONSTANTNUMBER3 {
		return nil, errors.Errorf(constant.ERRCODE4, "The account has been transferred, please activate first : %v", err)
	}

	//   冻结状态 0-正常;1-合约冻结/账户冻结;2-金额冻结;3-暂禁
	if querySSV9000004O.Records[0].FreezeType == constant.CONSTANTNUMBER1 {
		return nil, errors.Errorf(constant.ERRCODE5, "Account is frozen : %v", err)
	}else if querySSV9000004O.Records[0].FreezeType == constant.CONSTANTNUMBER3 {
		return nil, errors.Errorf(constant.ERRCODE6, "Account is temporarily banned : %v", err)
	}

	//客户信息查询BFS
	//从个人合约表拿到客户号作为入参，返回客户状态
	querySSV1000012, err := impl.querySSV1000012(querySSV9000004O.Records[0].CstmrId)
	if err != nil {
		return nil, err
	}

	// 0-正常 1-死亡 2-破产 3-暂禁 4-待生效 9-注销
	if querySSV1000012.CustStatus == constant.CONSTANTNUMBER3 {
		return nil, errors.Errorf(constant.ERRCODE7, "Customer has been temporarily banned : %v", err)
	}

	//合约信息更新
	err = impl.updateSSV9000005()
	if err != nil {
		return nil, err
	}

	//客户合约更新BFS
	err = impl.updateSSV1000013(querySSV9000004O.Records[0].CstmrId)
	if err != nil {
		return nil, err
	}

	ssv0000004O = &models.SSV0000004O{
		//Status: "ok",
	}
	log.Debug("End try ssv0000004")
	return ssv0000004O, nil
}

func (impl *Ssv0000004Impl) ConfirmSsv0000004(ssv0000004I *models.SSV0000004I) (ssv0000004O *models.SSV0000004O, err error) {
	log.Debug("Start confirm ssv0000004")
	return nil, nil
}

func (impl *Ssv0000004Impl) CancelSsv0000004(ssv0000004I *models.SSV0000004I) (ssv0000004O *models.SSV0000004O, err error) {
	log.Debug("Start cancel ssv0000004")
	return nil, nil
}

//合约信息查询 ssv9000004
func (impl *Ssv0000004Impl) querySSV9000004() (*models.SSV9000004O, error) {
	ssv9000004I := models.SSV9000004I{
		QryTpy:        constant.CONSTANTNUMBER1,     //查询类型
		AgreementID:   impl.Sv000004I.AgreementID,   //合约号
		AgreementType: impl.Sv000004I.AgreementType, //合约类型
	}
	requestBody, err := models.PackRequest(ssv9000004I)
	if err != nil {
		return nil, errors.Errorf(constant.ERRCODE_IL9, "%v", err)
	}
	responseBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_AGM, impl.Sv000004I.AgreementID, constant.SV900004, requestBody)
	if err != nil {
		return nil, errors.Errorf(constant.ERRCODE8, "Contract information query failed : %v", err)
	}

	SSV9000004O := models.SSV9000004O{}
	err = SSV9000004O.UnPackResponse(responseBody)
	if err != nil {
		return nil, errors.Errorf(constant.ERRCODE_IL12, "%v", err)
	}
	return &SSV9000004O, nil
}

//客户信息查询BFS
func (impl *Ssv0000004Impl) querySSV1000012(custId string) (*models.SSV1000012O, error) {
	ssv1000012I := models.SSV1000012I{
		CustId: custId, //客户号
	}
	requestBody, err := models.PackRequest(ssv1000012I)
	if err != nil {
		return nil, errors.Errorf(constant.ERRCODE_IL9, "Packing failed : %v", err)
	}
	responseBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constant.SV100012, requestBody)
	if err != nil {
		return nil, errors.Errorf(constant.ERRCODE9, "Customer information query BFS failed : %v", err)
	}

	SSV1000012O := models.SSV1000012O{}
	err = SSV1000012O.UnPackResponse(responseBody)
	if err != nil {
		return nil, errors.Errorf(constant.ERRCODE_IL12, "UnPacking failed : %v", err)
	}

	return &SSV1000012O, nil
}

//客户合约更新BFS
func (impl *Ssv0000004Impl) updateSSV1000013(custId string) error {
	ssv1000013I := models.SSV1000013I{
		CustId:        custId,                     //客户号
		Account:       impl.Sv000004I.AgreementID, //合约号
		AccuntNme:     impl.Sv000004I.AccuntNme,
		CstmrCntctPh:  impl.Sv000004I.CstmrCntctPh,
		CstmrCntctAdd: impl.Sv000004I.CstmrCntctAdd,
		CstmrCntctEm:  impl.Sv000004I.CstmrCntctEm,
		//ObjPublic:     models.SSV1000013IObjPublic{
		//	BranchNo: "",
		//	TellerId: "",
		//},
	}
	requestBody, err := models.PackRequest(ssv1000013I)
	if err != nil {
		return errors.Errorf(constant.ERRCODE_IL9, "%v", err)
	}
	_, err = impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constant.SV100013, requestBody)
	if err != nil {
		return errors.Errorf(constant.ERRCODE10, "Customer contract update BFS failed : %v", err)
	}

	return nil
}

//合约信息更新
func (impl *Ssv0000004Impl) updateSSV9000005() error {
	ssv9000005I := models.SSV9000005I{
		AgreementID:   impl.Sv000004I.AgreementID,   //合约号
		AgreementType: impl.Sv000004I.AgreementType, //合约类型
		AccuntNme:     impl.Sv000004I.AccuntNme,     //账户名称
		CstmrCntctPh:  impl.Sv000004I.CstmrCntctPh,  //手机号码
		CstmrCntctAdd: impl.Sv000004I.CstmrCntctAdd, //联系地址
		CstmrCntctEm:  impl.Sv000004I.CstmrCntctEm,  //邮箱
	}
	requestBody, err := models.PackRequest(ssv9000005I)
	if err != nil {
		return errors.Errorf(constant.ERRCODE_IL9, "%v", err)
	}
	_, err = impl.RequestSyncServiceElementKey(constant.DLS_TYPE_AGM, impl.Sv000004I.AgreementID, constant.SV900005, requestBody)
	if err != nil {
		return errors.Errorf(constant.ERRCODE11, "updateSSV9000005 failed : %v", err)
	}

	return nil
}
