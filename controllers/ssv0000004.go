//Version: v0.0.1
package controllers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/isaving/sv/ssv0000004/models"
	"git.forms.io/isaving/sv/ssv0000004/services"
	 commclient "git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv0000004Controller struct {
    controllers.CommTCCController
}

func (*Ssv0000004Controller) ControllerName() string {
	return "Ssv0000004Controller"
}

// @Desc ssv0000004 controller
// @Description Entry
// @Param ssv0000004 body models.SSV0000004I true "body for user content"
// @Success 200 {object} models.SSV0000004O
// @router /ssv0000004 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv0000004Controller) Ssv0000004() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv0000004Controller.Ssv0000004 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv0000004I := &models.SSV0000004I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv0000004I); err != nil {
		c.SetServiceError(err)
		return
	}
  if err := ssv0000004I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv0000004 := &services.Ssv0000004Impl{}
    ssv0000004.New(c.CommTCCController)
    ssv0000004.Sv000004I = ssv0000004I
	ssv0000004.DlsInterface = &commclient.DlsOperate{}
	ssv0000004Compensable := services.Ssv0000004Compensable

	proxy, err := aspect.NewDTSProxy(ssv0000004, ssv0000004Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv0000004I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD,"DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv0000004O, ok := rsp.(*models.SSV0000004O); ok {
		if responseBody, err := models.PackResponse(ssv0000004O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
} 
// @Title Ssv0000004 Controller
// @Description ssv0000004 controller
// @Param Ssv0000004 body models.SSV0000004I true body for SSV0000004 content
// @Success 200 {object} models.SSV0000004O
// @router /create [post]
/*func (c *Ssv0000004Controller) SWSsv0000004() {
	//Here is to generate API documentation, no need to implement methods
}*/
